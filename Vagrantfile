# -*- mode: ruby -*-
# vi: set ft=ruby :

machines = {
  "awx"  => {"memory" => "4096", "cpu" => "2", "ip" => "10", "image" => "ubuntu/bionic64"},
  "centos" => {"memory" => "1024", "cpu" => "1", "ip" => "20", "image" => "centos/7"},
  "debian" => {"memory" => "1024", "cpu" => "1", "ip" => "30", "image" => "debian/buster64"}
}

Vagrant.configure("2") do |config|
  machines.each do |name, conf|
    config.vm.define "#{name}" do |machine|
      machine.vm.box = "#{conf["image"]}"
      machine.vm.hostname = "#{name}.local"
      machine.vm.network "private_network", ip: "10.10.10.#{conf["ip"]}"

      machine.vm.provider "virtualbox" do |vb|
        vb.name = "#{name}"
        vb.memory = conf["memory"]
        vb.cpus = conf["cpu"]
      end

      if "#{conf["image"]}" == "ubuntu/bionic64"
        machine.vm.provision "ansible" do |ansible|
          ansible.playbook = "provision/playbook.yml"
        end
      end

      if "#{conf["image"]}" == "centos/7"
        machine.vm.provision "shell", inline: <<-SHELL
          sudo yum install python3 -y
        SHELL
      end

      if "#{conf["image"]}" == "centos/7" or "#{conf["image"]}" == "debian/buster64"
        machine.vm.provision "file", source: "awx.pub", destination: "~/.ssh/awx.pub"
        machine.vm.provision "shell", inline: <<-SHELL
          cat /home/vagrant/.ssh/awx.pub >> /home/vagrant/.ssh/authorized_keys
        SHELL
      end
    end
  end
end
