## AWX Lab Example


First generate ssh keys:
  - `ssh-keygen -q -f awx -P ""`

Deploy machines with vagrant:
  - `vagrant up`

AWX can be acessed in 10.10.10.10 with user admin and password admin, and hosts centos: 10.10.10.20, and debian: 10.10.10.30.

Testing a ping with Ansible:
  - `ansible servers -i hosts --private-key=awx -m ping` or simple `ansible servers -m ping`

Filter something in getting facts:
  - `ansible servers -m setup -a "filter=some_filter"`

Running a playbook:
  - `ansible-playbook -i hosts --private-key=awx playbook.yml` or simple `ansible-playbook playbook-example.yml`
